import sys
from typing import Union

import time

from PyQt5.QtWidgets import QApplication, QWidget, QMenu, QInputDialog, QAction, QMainWindow, QMenuBar, QFileDialog, QAction
from PyQt5.QtGui import QPainter, QPen, QMouseEvent, QPaintEvent, QKeyEvent, QContextMenuEvent, QFont, QColor
from PyQt5.QtCore import Qt, QRect
from PyQt5.QtTest import QTest
from graph import GraphData, Point, Vertex, Edge
from algs import GraphAlgorithms

import numpy as np

def cap(val, min, max):
    if val < min:
        return min
    elif val > max:
        return max
    else:
        return val


class GraphDisplay(QMainWindow):
    DOT_SIZE = 1000
    VERTEX_SIZE = 10
    EDGE_WIDTH = 4
    TEXT_WIDTH = 100
    TEXT_SIZE = 32

    REPAINT_FPS = 60
    SELECT_VERTEX_ACCURACY_SQ = 200
    SELECT_EDGE_ACCURACY = 10

    STATE_EDIT = 1
    STATE_DISPLAY = 2
    STATE_RUN = 3

    def __init__(self):
        super().__init__()
        self.text = "Test display"
        self.setGeometry(300, 300, 280, 170)
        self.setWindowTitle('Graph display')
        self.setMouseTracking(True)
        self.show()

        self._initMenuBar()
        self._initGraphData()

        self.lastRepaintTime = time.time()
        self.state = GraphDisplay.STATE_EDIT

    def _initGraphData(self):
        self.selectedVertex: Union[Vertex, None] = None
        self.selectedMoveVertex: Union[Vertex, None] = None
        self.selectedMoveVertexMoved: bool = False
        self.lastMousePosition: Point = Point(0, 0)

        self.statusBarContent = ""
        self.statusBarType = GraphAlgorithms.MESS_STATUS

        self.graph_data = GraphData()
        self.algs = GraphAlgorithms(self.graph_data)

    def _initMenuBar(self):
        bar = self.menuBar()

        m_file = bar.addMenu("File")
        a_open = QAction("Open", self)
        a_open.triggered.connect(self._open_file)
        m_file.addAction(a_open)
        a_save = QAction("Save", self)
        a_save.triggered.connect(self._save_to_file)
        m_file.addAction(a_save)

        callalgs = lambda bind: lambda: self._exec_alg(bind)
        m_gen = bar.addMenu("Generate")
        a_autoname = QAction("Automatic names", self)
        a_autoname.triggered.connect(callalgs('autoname'))
        m_gen.addAction(a_autoname)
        a_autoweight = QAction("Automatic weights", self)
        a_autoweight.triggered.connect(callalgs('autoweight'))
        m_gen.addAction(a_autoweight)

        m_exec = bar.addMenu("Execute")
        a_prims = QAction("Prim's algorithm", self)
        a_prims.triggered.connect(callalgs('prims'))
        m_exec.addAction(a_prims)

    def _exec_alg(self, bind):
        orig_state = self.state
        self._set_state(self.STATE_RUN)
        for m in self.algs.bindings[bind]():
            if self.state != self.STATE_RUN:
                break
            cmd = m[0]
            args = m[1:]
            if cmd == GraphAlgorithms.MESS_END:
                break
            elif cmd == GraphAlgorithms.MESS_END_SILENT:
                self._set_state(orig_state)
                break
            elif cmd == GraphAlgorithms.MESS_UPDATE:
                self.update()
            elif cmd == GraphAlgorithms.MESS_STATUS or cmd == GraphAlgorithms.MESS_ERR:
                self.statusBarType = cmd
                self.statusBarContent = args[0]
                self.update()
            elif cmd == GraphAlgorithms.MESS_STEP:
                self.update()
                # TODO take into account args[0] which is the priority
                # TODO implement stepping mechanism with spacebar
                QTest.qWait(1000)
            else:
                print('Unknown message type from algorithm manager', m)

        self.update()

    def _open_file(self):
        name = QFileDialog.getOpenFileName(self, 'Open graph', '', 'Graph (*.json)')
        if name is not None:
            name = name[0]
            with open(name, 'r') as f:
                self.graph_data = GraphData.decode(f.read())
                self.algs = GraphAlgorithms(self.graph_data)
                self.state = GraphDisplay.STATE_DISPLAY
        self.update()

    def _save_to_file(self):
        name = QFileDialog.getSaveFileName(self, 'Save graph', '', 'Graph (*.json)')
        if name is not None:
            name = name[0]
            if name[-5:] != '.json':
                name += '.json'
            with open(name, 'w') as f:
                f.write(self.graph_data.encode())

    def paintEvent(self, event: QPaintEvent):
        _highlight_color = QColor(255, 168, 0)
        _highlight2_color = QColor(255, 232, 64)
        _faded_color = QColor(200, 200, 200)
        _err_color = QColor(255, 30, 30)
        _base_color = QColor('black')

        qp = QPainter()
        qp.begin(self)
        def _set_color(color):
            pen = QPen()
            pen.setWidth(GraphDisplay.EDGE_WIDTH)
            pen.setColor(color)
            qp.setPen(pen)
            qp.setBrush(color)
        _set_color(_base_color)
        font = QFont()
        font.setPointSize(18)
        qp.setFont(font)

        for e in self.graph_data.get_edges():
            if e.data.get('hide'): continue
            if e.data.get('faded'):
                _set_color(_faded_color)
            elif e.data.get('highlight'):
                _set_color(_highlight_color)
            elif e.data.get('highlight2'):
                _set_color(_highlight2_color)
            else:
                _set_color(_base_color)
            x1, y1 = self._from_local_coord(self.graph_data.get_vertex(e.left).get_pos())
            x2, y2 = self._from_local_coord(self.graph_data.get_vertex(e.right).get_pos())
            qp.drawLine(x1, y1, x2, y2)
            if e.weight is not None and not e.data.get('faded'):
                x3, y3 = (x1+x2)/2, (y1+y2)/2
                perp = np.array([y2-y1, x1-x2])
                perp_n = perp / np.linalg.norm(perp)
                perp_n *= np.sign(np.dot(perp_n, [0,-1]))
                qp.drawText(QRect(x3 - GraphDisplay.TEXT_WIDTH // 2 + perp_n[0]*GraphDisplay.TEXT_SIZE, y3 - 6 + perp_n[1]*GraphDisplay.TEXT_SIZE, GraphDisplay.TEXT_WIDTH, GraphDisplay.TEXT_SIZE), Qt.AlignCenter, str(e.weight))

        # vertices drawn after edges so they draw over different colors
        for v in self.graph_data.get_vertices():
            if v.data.get('hide'): continue
            if v.data.get('faded'):
                _set_color(_faded_color)
            elif v.data.get('highlight'):
                _set_color(_highlight_color)
            elif v.data.get('highlight2'):
                _set_color(_highlight2_color)
            else:
                _set_color(_base_color)
            x, y = self._from_local_coord(v.get_pos())
            qp.drawEllipse(x - GraphDisplay.VERTEX_SIZE // 2, y - GraphDisplay.VERTEX_SIZE // 2, GraphDisplay.VERTEX_SIZE, GraphDisplay.VERTEX_SIZE)
            if v.name != "" and not v.data.get('faded'):
                qp.drawText(QRect(x - GraphDisplay.TEXT_WIDTH // 2, y - GraphDisplay.VERTEX_SIZE - GraphDisplay.TEXT_SIZE, GraphDisplay.TEXT_WIDTH, GraphDisplay.TEXT_SIZE), Qt.AlignCenter, v.name)

        _set_color(_base_color)

        if self.selectedVertex is not None:
            qp.drawLine(*self._from_local_coord(self.selectedVertex.get_pos()), *self._from_local_coord(self.lastMousePosition))

        width = self.size().width()
        height = self.size().height()

        qp.drawText(QRect(10, height - GraphDisplay.TEXT_SIZE - 10, GraphDisplay.TEXT_WIDTH, GraphDisplay.TEXT_SIZE), Qt.AlignLeft, {GraphDisplay.STATE_EDIT: 'Edit', GraphDisplay.STATE_DISPLAY: 'Display', GraphDisplay.STATE_RUN: 'Run'}.get(self.state))

        if self.statusBarType == GraphAlgorithms.MESS_STATUS:
            _set_color(_base_color)
        elif self.statusBarType == GraphAlgorithms.MESS_ERR:
            _set_color(_err_color)
        qp.drawText(QRect(GraphDisplay.TEXT_SIZE + 20, height - GraphDisplay.TEXT_SIZE - 10, width - GraphDisplay.TEXT_SIZE - 30, GraphDisplay.TEXT_SIZE), Qt.AlignCenter, self.statusBarContent)

        qp.end()

    def _set_state(self, new):
        if self.state == GraphDisplay.STATE_EDIT:
            self.selectedVertex = None
            self.selectedMoveVertex = None
            self.selectedMoveVertexMoved = False
        elif self.state == GraphDisplay.STATE_RUN:
            for v in self.graph_data.get_vertices():
                v.data.clear()
            for e in self.graph_data.get_edges():
                e.data.clear()
            self.statusBarType = None
            self.statusBarContent = ""

        self.state = new

    def mousePressEvent(self, evt: QMouseEvent):
        local_pos = self._to_local_coord(Point(evt.x(), evt.y()))
        close = self._find_vector_close_to(local_pos)
        if evt.button() == Qt.LeftButton and self.state == GraphDisplay.STATE_EDIT:
            if close is None:
                close = self.graph_data.add_vertex_id(local_pos)
            self.selectedVertex = close
        if evt.button() == Qt.RightButton and self.state == GraphDisplay.STATE_EDIT:
            if close is not None:
                self.selectedMoveVertex = close
                self.selectedMoveVertexMoved = False
        self.update()

    def mouseReleaseEvent(self, evt: QMouseEvent):
        if self.selectedVertex is None and (self.selectedMoveVertex is None or not self.selectedMoveVertexMoved):
            self.showContextMenu(evt)
        if self.selectedVertex is not None:
            closest = self._find_vector_close_to(self._to_local_coord(Point(evt.x(), evt.y())))
            if closest is not None and closest != self.selectedVertex and self.state == GraphDisplay.STATE_EDIT:
                self.graph_data.add_edge_id(self.selectedVertex.id, closest.id)
            self.selectedVertex = None
        if self.selectedMoveVertex is not None:
            self.selectedMoveVertex = None
        self.update()

    def mouseMoveEvent(self, evt: QMouseEvent):
        self.lastMousePosition = self._within_coords(self._to_local_coord(Point(evt.x(), evt.y())))
        if self.selectedMoveVertex is not None:
            self.selectedMoveVertex.pos = self.lastMousePosition
            self.selectedMoveVertexMoved = True
        if time.time() - self.lastRepaintTime > 1.0 / self.REPAINT_FPS:
            self.lastRepaintTime = time.time()
            self.repaint()

    def keyPressEvent(self, evt: QKeyEvent):
        if self.state != GraphDisplay.STATE_DISPLAY and (evt.key() == Qt.Key_Escape or evt.key() == Qt.Key_Q):
            self._set_state(GraphDisplay.STATE_DISPLAY)
        if self.state == GraphDisplay.STATE_DISPLAY and evt.key() == Qt.Key_E:
            self._set_state(GraphDisplay.STATE_EDIT)
        self.update()

    def showContextMenu(self, evt: QContextMenuEvent) -> None:
        local_pos = self._to_local_coord(Point(evt.x(), evt.y()))
        close_vec = self._find_vector_close_to(local_pos)
        close_edge = self._find_edge_close_to(local_pos)

        menu = QMenu(self)
        if self.state == GraphDisplay.STATE_EDIT:
            if close_vec is not None:
                menu.addAction("Name vertex")
                menu.addAction("Remove vertex")
            elif close_edge is not None:
                menu.addAction("Set weight")
                menu.addAction("Remove edge")
            else:
                menu.addAction("Create vertex")

        action = menu.exec(self.mapToGlobal(evt.pos()))
        if action is None:
            return

        def name_vertex(v: Vertex):
            dialog = QInputDialog(self)
            dialog.setLabelText("Name:")
            dialog.exec()
            v.name = dialog.textValue()
        def edge_weight(e: Edge):
            dialog = QInputDialog(self)
            dialog.setLabelText("Weight:")
            dialog.exec()
            try:
                e.weight = int(dialog.textValue())
            except Exception:
                e.weight = None
        if action.text() == "Create vertex":
            v = self.graph_data.add_vertex_id(local_pos)
            name_vertex(v)
        elif action.text() == "Name vertex":
            name_vertex(close_vec)
        elif action.text() == "Remove vertex":
            self.graph_data.remove_vertex_id(close_vec.id)
        elif action.text() == "Remove edge":
            self.graph_data.remove_edge_id(close_edge.left, close_edge.right)
        elif action.text() == "Set weight":
            edge_weight(close_edge)

        self.update()

    def _find_vector_close_to(self, p: Point):
        closest, min_dist_sq = None, self.SELECT_VERTEX_ACCURACY_SQ
        for v in self.graph_data.get_vertices():
            x, y = v.get_pos()
            cur_dist_sq = (p.x - x)**2 + (p.y - y)**2
            if cur_dist_sq < min_dist_sq:
                closest = v
                min_dist_sq = cur_dist_sq
        return closest

    def _find_edge_close_to(self, p: Point):
        closest, min_dist = None, self.SELECT_EDGE_ACCURACY
        for e in self.graph_data.get_edges():
            p1 = self.graph_data.get_vertex(e.left).get_pos()
            p2 = self.graph_data.get_vertex(e.right).get_pos()
            p, p1, p2 = map(np.array, [p, p1, p2])
            dist = np.linalg.norm(np.cross(p2-p1, p1-p))/np.linalg.norm(p2-p1)
            if dist < min_dist:
                closest = e
                min_dist = dist
        return closest

    def _to_local_coord(self, p: Point) -> Point:
        return Point((p[0] * self.DOT_SIZE) // self.size().width(), (p[1] * self.DOT_SIZE) // self.size().height())

    def _from_local_coord(self, p: Point) -> Point:
        return Point((self.size().width() * p[0]) // self.DOT_SIZE, (self.size().height() * p[1]) // self.DOT_SIZE)

    def _within_coords(self, p: Point) -> Point:
        return Point(cap(p.x, 0, self.DOT_SIZE), cap(p.y, 0, self.DOT_SIZE))


if __name__ == '__main__':
    app = QApplication(sys.argv)
    gp = GraphDisplay()
    sys.exit(app.exec_())

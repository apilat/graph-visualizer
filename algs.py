import random

class GraphAlgorithms:
    MESS_END = 1
    MESS_END_SILENT = 6 # silent end reverts to previous state without user interaction
    MESS_UPDATE = 2
    MESS_STATUS = 3
    MESS_ERR = 4
    MESS_STEP = 5

    def __init__(self, graphdata):
        self.graph = graphdata
        self.bindings = {
                'autoname': self.autoname,
                'autoweight': self.autoweight,
                'prims': self.prims_algorithm,
                }
        self.state = {}

    def reset(self):
        self.state.clear()

    def autoname(self):
        cur = self.state.get('autoname:cur', 0)
        for v in self.graph.get_vertices():
            if v.name == "":
                v.name = chr(cur + ord('A'))
                cur += 1
        self.state['autoname:cur'] = cur
        yield (self.MESS_END_SILENT,)

    def autoweight(self):
        limit = len(self.graph.get_vertices()) * 2
        for e in self.graph.get_edges():
            if e.weight is None:
                e.weight = random.randint(1, limit)
        yield (self.MESS_END_SILENT,)

    def prims_algorithm(self):
        fail = False
        for e in self.graph.get_edges():
            if e.weight is None:
                if not fail:
                    yield (self.MESS_ERR, "Not all edges have weight")
                fail = True
        if len(self.graph.get_vertices()) < 1:
            yield (self.MESS_ERR, "No vertices")
            fail = True

        # TODO fail if not connected
        if fail:
            yield (self.MESS_END,)

        for e in self.graph.get_edges():
            e.data['faded'] = True
        yield (self.MESS_UPDATE,)

        total_weight = 0

        out_set = set(self.graph.get_vertices())
        first = out_set.pop()
        in_set = set((first,))
        # TODO do weight sorting using a priority queue
        path = {}
        for n in first.get_neighbors():
            v = self.graph.get_vertex(n)
            edge = self.graph.get_edge(first.id, n)
            path[v] = edge

        first.data['highlight'] = True
        yield (self.MESS_STATUS, f"Starting algorithm from {first.describe()}")
        yield (self.MESS_STEP, 2)
        for n in first.get_neighbors():
            e = self.graph.get_edge(first.id, n)
            e.data.pop('faded')
            e.data['highlight2'] = True
        yield (self.MESS_STEP, 1)

        while out_set:
            cur = min(path.keys(), key=lambda v: path[v].weight)
            edge = path[cur]
            path.pop(cur)
            out_set.discard(cur)
            in_set.add(cur)
            total_weight += edge.weight

            yield (self.MESS_STATUS, f"Processing vertex {cur.describe()}")
            cur.data['highlight'] = True
            edge.data.pop('highlight2')
            edge.data['highlight'] = True
            yield (self.MESS_STEP, 2)

            for n in cur.get_neighbors():
                v = self.graph.get_vertex(n)
                e = self.graph.get_edge(n, cur.id)
                if v in out_set and (v not in path or e.weight < path[v].weight):
                    path[v] = e
                e.data.pop('faded', None)
                e.data['highlight2'] = True

            yield (self.MESS_STEP, 1)
        yield (self.MESS_STATUS, f"Found minimum spanning tree of weight {total_weight}")

from typing import Dict, Set, Tuple, NamedTuple, Union
import json

IdType = int

class Point(NamedTuple):
    x: int
    y: int

class Vertex:
    _ID_COUNT = 0

    def __init__(self, name: str, pos: Point, data):
        self.name = name
        self.adjacent: Set[IdType] = set()
        self.id = Vertex._ID_COUNT
        self.pos = pos
        self.data = data
        Vertex._ID_COUNT += 1

    def add_edge(self, vid: IdType):
        self.adjacent.add(vid)

    def remove_edge(self, vid: IdType):
        self.adjacent.remove(vid)

    def remove_edge_if_exists(self, vid: IdType):
        if vid in self.adjacent:
            self.adjacent.remove(vid)

    def is_adjacent(self, vid: IdType) -> bool:
        return vid in self.adjacent

    def get_neighbors(self) -> Set[IdType]:
        return self.adjacent

    def get_pos(self) -> Point:
        return self.pos

    def get_data(self):
        return self.data

    def encode(self):
        return json.dumps({'name': self.name, 'adjacent': list(self.adjacent), 'id': self.id, 'pos': self.pos, 'data': self.data})

    def decode(data):
        data = json.loads(data)
        v = Vertex('', None, None)
        v.name = data['name']
        v.adjacent = set(data['adjacent'])
        v.id = data['id']
        v.pos = data['pos']
        v.data = data['data']
        return v

    def describe(self):
        if self.name != "":
            return self.name
        else:
            return str(self.id)

class Edge:
    _ID_COUNT = 0

    def __init__(self, left: IdType, right: IdType, data, weight: Union[None, int]=None):
        self.left = left
        self.right = right
        self.weight = weight
        self.id = Edge._ID_COUNT
        self.data = data
        Edge._ID_COUNT += 1

    def get_weight() -> Union[None, int]:
        return self.weight

    def set_weight(weight: Union[None, int]):
        self.weight = weight

    def get_data():
        return self.data

    def encode(self):
        return json.dumps(self.__dict__)

    def decode(data):
        data = json.loads(data)
        e = Edge(None, None, None, None)
        e.left = data['left']
        e.right = data['right']
        e.weight = data['weight']
        e.id = data['id']
        e.data = data['data']
        return e

class GraphData:
    def __init__(self):
        self.vertices: Dict[IdType, Vertex] = {}
        self.edges: Dict[Tuple[IdType, IdType], Edge] = {}

    def add_vertex_id(self, pos=Point(0, 0), data=None) -> Vertex:
        if data is None:
            data = {}
        vertex = Vertex("", pos, data)
        self.vertices[vertex.id] = vertex
        return vertex

    def get_vertex(self, vid: IdType):
        return self.vertices[vid]

    def remove_vertex_id(self, vid: IdType):
        for tid in self.vertices[vid].get_neighbors():
            self.vertices[tid].remove_edge(vid)
            self._delete_edge_either_direction(vid, tid)

        self.vertices.pop(vid, None)

    def add_edge_id(self, fid: IdType, tid: IdType, data=None):
        self.vertices[fid].add_edge(tid)
        self.vertices[tid].add_edge(fid)
        if data is None:
            data = {}
        edge = Edge(fid, tid, data)
        self.edges[(fid, tid)] = edge

    def get_edge(self, fid: IdType, tid: IdType):
        return self._get_edge_either_direction(fid, tid)

    def remove_edge_id(self, fid: IdType, tid: IdType):
        self.vertices[fid].remove_edge(tid)
        self.vertices[tid].remove_edge(fid)
        self._delete_edge_either_direction(fid, tid)

    def set_edge_weight(self, fid: IdType, tid: IdType, weight: int):
        e = self._get_edge_either_direction(fid, tid)
        e.set_weight(weight)

    def _get_edge_either_direction(self, fid: IdType, tid: IdType):
        e = self.edges.get((fid, tid))
        if e is None:
            e = self.edges.get((tid, fid))
        return e

    def _delete_edge_either_direction(self, fid: IdType, tid: IdType):
        self.edges.pop((fid, tid), None)
        self.edges.pop((tid, fid), None)

    def is_adjacent_id(self, fid: IdType, tid: IdType):
        return self.vertices[fid].is_adjacent(tid)

    def get_neighbors_id(self, vid: IdType):
        return self.vertices[vid].get_neighbors()

    def get_vertices(self) -> Set[Vertex]:
        return self.vertices.values()

    def get_edges(self) -> Set[Edge]:
        return self.edges.values()

    def encode(self) -> str:
        verts = list(map(Vertex.encode, self.vertices.values()))
        edges = list(map(Edge.encode, self.edges.values()))
        return json.dumps({'vertices': verts, 'edges': edges})

    def decode(data: str):
        data = json.loads(data)
        graph = GraphData()
        for v in data['vertices']:
            vd = Vertex.decode(v)
            graph.vertices[vd.id] = vd
        for e in data['edges']:
            ed = Edge.decode(e)
            graph.edges[(ed.left, ed.right)] = ed
        return graph
